#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScreen>
#include <QRegExpValidator>
#include "morse_translator.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    MorseTranslator _morseTranslator;

private slots:
    void on_plainLineEdit_textEdited(const QString &text);
    void on_morseLineEdit_textEdited(const QString &text);

private:
    Ui::MainWindow *ui;
    void _moveToScreenCenter();
};
#endif // MAINWINDOW_H
