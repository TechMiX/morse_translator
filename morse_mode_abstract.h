#ifndef MORSEMODEABSTRACT_H
#define MORSEMODEABSTRACT_H

#include <QString>
#include <QHash>

class MorseModeAbstract {
public:
    QString morseOf(QString plain);
    QString plainOf(QString morse);
    virtual void populateMappings() = 0;
    virtual ~MorseModeAbstract() {}

protected:
    QHash<QString, QString> _plainToMorseMap;
    QHash<QString, QString> _morseToPlainMap;
};

#endif // MORSEMODEABSTRACT_H
