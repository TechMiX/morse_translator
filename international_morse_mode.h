#ifndef INTERNATIONALMORSEMODE_H
#define INTERNATIONALMORSEMODE_H

#include "morse_mode_abstract.h"

class InternationalMorseMode : public MorseModeAbstract
{
public:
    InternationalMorseMode();
    ~InternationalMorseMode();
    void populateMappings();
};

#endif // INTERNATIONALMORSEMODE_H
