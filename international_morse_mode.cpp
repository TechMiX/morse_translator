#include "international_morse_mode.h"

InternationalMorseMode::InternationalMorseMode()
{
    populateMappings();
}

InternationalMorseMode::~InternationalMorseMode()
{
}

void InternationalMorseMode::populateMappings()
{
    QStringList alphabet = QStringList({
                                           "a", "b", "c", "d", "e",
                                           "f", "g", "h", "i", "j",
                                           "k", "l", "m", "n", "o",
                                           "p", "q", "r", "s", "t",
                                           "u", "v", "w", "x", "y",
                                           "z", "1", "2", "3", "4",
                                           "5", "6", "7", "8", "9",
                                           "0"
                                       });
    QStringList codes = QStringList({
                                        ".-", "-...", "-.-.", "-..", ".",
                                        "..-.", "--.", "....", "..", ".---",
                                        "-.-", ".-..", "--", "-.", "---",
                                        ".--.", "--.-", ".-.", "...", "-",
                                        "..-", "...-", ".--", "-..-", "-.--",
                                        "--..", ".----", "..---", "...--", "....-",
                                        ".....", "-....", "--...", "---..", "----.",
                                        "-----"
                                    });

    for (int i=0; i<alphabet.length(); i++) {
        _plainToMorseMap.insert(alphabet.at(i), codes.at(i));
        _morseToPlainMap.insert(codes.at(i), alphabet.at(i));
    }
}
