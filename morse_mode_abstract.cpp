#include "morse_mode_abstract.h"

QString MorseModeAbstract::morseOf(QString plain)
{
    return _plainToMorseMap.value(plain, "");
}

QString MorseModeAbstract::plainOf(QString morse)
{
    return _morseToPlainMap.value(morse, "");
}
