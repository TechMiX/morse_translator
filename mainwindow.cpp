#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->plainLineEdit->setValidator( new QRegExpValidator( QRegExp("[A-Za-z0-9 ]*") ) );
    ui->morseLineEdit->setValidator( new QRegExpValidator( QRegExp("[.- ]*") ) );

    _moveToScreenCenter();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::_moveToScreenCenter()
{
    QRect position = this->frameGeometry();
    QPoint screenCenter = QGuiApplication::screens().front()->availableGeometry().center();
    position.moveCenter( screenCenter );
    this->move( position.topLeft() );
}

void MainWindow::on_plainLineEdit_textEdited(const QString &text)
{
    ui->morseLineEdit->setText( _morseTranslator.fromPlain(text) );
}

void MainWindow::on_morseLineEdit_textEdited(const QString &text)
{
    ui->plainLineEdit->setText( _morseTranslator.fromMorse(text) );
}
