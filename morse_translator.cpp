#include "morse_translator.h"

MorseTranslator::MorseTranslator() : MorseTranslator(new InternationalMorseMode())
{
}

MorseTranslator::MorseTranslator(MorseModeAbstract* morseMode) : _morseMode(morseMode)
{
}

MorseTranslator::~MorseTranslator()
{
    delete _morseMode;
}

QString MorseTranslator::fromPlain(QString plainText)
{
    QString result;
    plainText = plainText.toLower();
    for (int i=0; i<plainText.length(); i++) {
        result += _morseMode->morseOf(plainText.at(i));
        result += " ";
    }
    return result.trimmed();
}

QString MorseTranslator::fromMorse(QString morseText)
{
    QString result;
    QStringList morseSplited = morseText.split(" ");
    for (int i=0; i<morseSplited.count(); i++) {
        result += _morseMode->plainOf(morseSplited.at(i)).leftJustified(1);
    }
    return result.trimmed();
}
