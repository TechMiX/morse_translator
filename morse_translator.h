#ifndef MORSETRANSLATORH
#define MORSETRANSLATORH

#include <QString>
#include <QHash>
#include "morse_mode_abstract.h"
#include "international_morse_mode.h"

class MorseTranslator
{
public:
    MorseTranslator();
    ~MorseTranslator();
    MorseTranslator(MorseModeAbstract* morseMode);
    QString fromPlain(QString plainText);
    QString fromMorse(QString morseText);

private:
    MorseModeAbstract* _morseMode;
};

#endif // MORSETRANSLATORH
